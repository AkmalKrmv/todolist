var gulp         = require('gulp'), // Подключаем Gulp
  sass         = require('gulp-sass'), //Подключаем Sass пакет,
  scss         = require('scss'), //Подключаем Sass пакет,
  browserSync  = require('browser-sync'), // Подключаем Browser Sync
  concat       = require('gulp-concat'), // Подключаем gulp-concat (для конкатенации файлов)
  uglify       = require('gulp-uglifyjs'), // Подключаем gulp-uglifyjs (для сжатия JS)
  cssnano      = require('gulp-cssnano'), // Подключаем пакет для минификации CSS
  rename       = require('gulp-rename'), // Подключаем библиотеку для переименования файлов
  del          = require('del'), // Подключаем библиотеку для удаления файлов и папок
  imagemin     = require('gulp-imagemin'), // Подключаем библиотеку для работы с изображениями
  pngquant     = require('imagemin-pngquant'), // Подключаем библиотеку для работы с png
  cache        = require('gulp-cache'), // Подключаем библиотеку кеширования
  autoprefixer = require('gulp-autoprefixer');// Подключаем библиотеку для автоматического добавления префиксов

gulp.task('sass', function(){
  return gulp.src('assets/style/**/*.scss')
    .pipe(sass())
    .pipe(autoprefixer(['last 15 versions', '> 1%', 'ie 8', 'ie 7'], { cascade: true }))
    .pipe(gulp.dest('assets/style/'))
    .pipe(browserSync.reload({stream: true}))
});

gulp.task('browser-sync', function() { // Создаем таск browser-sync
  browserSync({ // Выполняем browserSync
    server: { // Определяем параметры сервера
      baseDir: './' // Директория для сервера
    },
    notify: false // Отключаем уведомления
  });
});
gulp.task('scripts', function() {
  return gulp.src([ // Берем все необходимые библиотеки
    'assets/js/**/*.js'
  ])
    .pipe(gulp.dest('assets/js/')); // Выгружаем в папку assets/js
});

/* gulp.task('scripts', function() {
 return gulp.src([ // Берем все необходимые библиотеки
 'assets/libs/jquery/dist/jquery.min.js', // Берем jQuery
 'assets/libs/magnific-popup/dist/jquery.magnific-popup.min.js' // Берем Magnific Popup
 ])
 .pipe(concat('libs.min.js')) // Собираем их в кучу в новом файле libs.min.js
 .pipe(uglify()) // Сжимаем JS файл
 .pipe(gulp.dest('assets/js')); // Выгружаем в папку assets/js
 });*/

/*gulp.task('css-libs', ['sass'], function() {
 return gulp.src('assets/css/libs.css') // Выбираем файл для минификации
 .pipe(cssnano()) // Сжимаем
 .pipe(rename({suffix: '.min'})) // Добавляем суффикс .min
 .pipe(gulp.dest('assets/style')); // Выгружаем в папку assets/css
 });*/

gulp.task('watch', ['browser-sync', 'sass'], function() {
  gulp.watch('assets/style/**/*.scss', ['sass']); // Наблюдение за sass файлами в папке sass
  gulp.watch('./*.html', browserSync.reload); // Наблюдение за HTML файлами в корне проекта
  gulp.watch('assets/js/**/*.js', browserSync.reload);   // Наблюдение за JS файлами в папке js
});





/* ----------------------------- Publish ------------------------------*/
gulp.task('clean', function() {
  return del.sync('Publish'); // Удаляем папку dist перед сборкой
});

gulp.task('img', function() {
  return gulp.src('assets/img/**/*') // Берем все изображения из assets
    .pipe(cache(imagemin({  // Сжимаем их с наилучшими настройками с учетом кеширования
      interlaced: true,
      progressive: true,
      svgoPlugins: [{removeViewBox: false}],
      use: [pngquant()]
    })))
    .pipe(gulp.dest('Publish/img')); // Выгружаем на продакшен
});

gulp.task('build', ['clean', 'img', 'sass', 'scripts'], function() {

  var buildCss = gulp.src([ // Переносим библиотеки в продакшен
    'assets/style/main.css',
    'assets/style/libs.min.css'
  ])
    .pipe(gulp.dest('Publish/css'));

  var buildFonts = gulp.src('assets/fonts/**/*') // Переносим шрифты в продакшен
    .pipe(gulp.dest('Publish/fonts'));

  var buildJs = gulp.src('assets/js/**/*') // Переносим скрипты в продакшен
    .pipe(gulp.dest('Publish/js'));

  var buildHtml = gulp.src('assets/*.html') // Переносим HTML в продакшен
    .pipe(gulp.dest('Publish'));

});

gulp.task('clear', function (callback) {
  return cache.clearAll();
});

gulp.task('default', ['watch']);
