// ignore this
require('./server-polyfills/String.prototype.endsWith');

/*var http = require('http');
var server = new http.Server(); //http.Server -> net.Server -> EventEmitter

server.listen(1212, '127.0.0.1');

// подписка на событие
server.on('request', function(req, res){
    res.writeHead(200, {'Content-Type':'text/plain'}); //!**  text-plain означает чистый текст
    res.end("Hello world");
});*/


/* The sedond variant */
const http = require('http');
const url = require('url');
const fs = require('fs');

//также лучше всего указать статусы 200 и 404 
//  а также какой тип данных мы передаем. 
//  Потому что это правильнее /!**

/**
 * Creating simple server wich returns files with appropriate mime types.
 *
 * Example:
 *  - /index.html
 *  - /assets/plugins/angular/angular.min.js
 *  - /assets/plugins/bootstrap/dist/css/bootstrap.min.css
 */
http.createServer(function (req, res) {

    var parsedUrl = url.parse(req.url, true); 
    var filename = "." + parsedUrl.pathname; // "." + "/index.html"
    var mimetype = getMimeType(parsedUrl.pathname);
    
    fs.readFile(filename, function (err, data) {
        if (err) {
            res.writeHead(404, { 'Content-Type': 'text/html' });
            return res.end("404 Not Found: " + filename);
        }

        res.writeHead(200, { 'Content-Type': mimetype });
        res.write(data);

        return res.end();
    });

}).listen(1212);

/**
 *  Hardcoded mime types.
 */
function getMimeType(filename) {
    if (filename.endsWith('.css'))
        return 'text/css';
    
    if (filename.endsWith('.js'))
        return 'application/javascript';

    return 'text/html';
}